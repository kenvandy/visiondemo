import {
  createSwitchNavigator,
  createAppContainer
} from 'react-navigation'
import AuthStack from '../views/Auth'
import MainStack from '../views/Main'

const AppNavigation = createSwitchNavigator({
  Auth: AuthStack,
  Main: MainStack
}, {
  initialRouteName: 'Auth',
})

export default createAppContainer(AppNavigation) 