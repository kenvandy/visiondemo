const colors = {
  whitePrimary: '#fff',
  darkPrimary: '#000',
  greenPrimary: '#287252',
}

export default colors