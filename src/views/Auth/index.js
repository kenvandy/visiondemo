import React, { Component } from 'react'
import {
  View,
  Text,
  ImageBackground,
  Image,
  StyleSheet
} from 'react-native'
import colors from '../../utils/colors';

export default class Auth extends Component {
  render() {
    return (
     <ImageBackground
      source={require('../../assets/images/loginBanner.jpg')}
      style={styles.container}
     >
      <View style={styles.loginWrapper}>
        <Image
          source={require('../../assets/images/Logo.png')}
          style={styles.logo}
        />


        <Text style={styles.welcomeTxt}>Welcome</Text>
        <Text style={styles.description}>We help you to take care your agriculture and espace you from the hard working</Text>
        
        <View style={styles.signUp}>
          <Text style={styles.signupTxt}>Sign up</Text>
        </View>
        <View style={styles.login}>
          <Text style={styles.loginTxt}>Login</Text>
        </View>
      </View>

     </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%'
  },
  loginWrapper: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center'
  },
  logo: {
    marginTop: 90,
    width: 80,
    height: 80
  },
  welcomeTxt: {
    color: colors.whitePrimary,
    marginTop: 40,
    textTransform: 'uppercase',
    fontWeight: '500',
    fontSize: 35,
    letterSpacing: 2
  },
  description: {
    color: colors.whitePrimary,
    width: '70%',
    textAlign: 'center',
    marginTop: 20,
    fontSize: 15,
    letterSpacing: 0.5,
    lineHeight: 25,
  },
  signUp: {
    marginTop: 100,
    width: '80%',
    height: 45,
    borderStyle: 'solid',
    borderColor: colors.whitePrimary,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20
  },
  signupTxt: {
    color: colors.whitePrimary,
    textTransform: 'uppercase',
    fontSize: 16,
    letterSpacing: 0.5
  },
  login: {
    marginTop: 30,
    width: '80%',
    height: 45,
    borderStyle: 'solid',
    borderColor: colors.whitePrimary,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: colors.whitePrimary,
  },
  loginTxt: {
    color: colors.greenPrimary,
    textTransform: 'uppercase',
    fontSize: 16,
    letterSpacing: 0.5
  }
})