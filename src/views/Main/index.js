import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation'
import CaptureScreen from './Capture'
import DetectScreen from './Detect'
import ResultScreen from './DetectResult'



const MainNavigation = createStackNavigator({
  capture: CaptureScreen,
  detect: DetectScreen,
  result: ResultScreen,

}, {
  initialRouteName: 'capture',
  headerMode: 'none',
})

export default createAppContainer(MainNavigation)